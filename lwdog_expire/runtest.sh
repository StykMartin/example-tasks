#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of run /tmp/c/a.out
#   Description: To cause local watchdog to expire for testing
#   Author: Carol Bouchard <cbouchar@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2016 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

rlJournalStart
    rlPhaseStartSetup
        rlRun "yum -y install gcc" 0 "Install C Compiler"
        rlRun "yum -y install tar" 0 "Install tar"
        rlRun "cp c.tar.gz /tmp;cd /tmp"
        rlRun "tar -xzf /tmp/c.tar.gz" 0 "untar C code"
        rlRun "touch /tmp/xyz;cd c;gcc test_sleep.c" 0 "Create sleeper"
    rlPhaseEnd
    rlPhaseStartTest
        rlRun /tmp/c/a.out
    rlPhaseEnd
# Print the test report
rlJournalPrintText
rlJournalEnd
